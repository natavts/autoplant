'use strict';

angular.module('autoplantApp')
    .controller('MainCtrl', function ($scope, $timeout) {
        $scope.humanlist = [];
        $scope.gearlist = [];
        $scope.hexlist = [];
        $scope.boxlist = [];
        $scope.items = {
            'h1': {'inBasket': '', 'inCorrectBasket': ''}
        };


        $scope.result = function () {
            //console.log('wow');
            var correct = 0;
            var dropBasket = 0;
            var outBasket = 0;
            for (var id in $scope.items) {
                //console.log('wow1');
                var itemsid = $scope.items[id];
                //console.log(itemsid);
                if (itemsid.inCorrectBasket) {
                    console.log(123);
                    correct++;
                    dropBasket++;
                } else if (itemsid.inBasket) {
                    dropBasket++;
                } else {
                    outBasket++;
                }
            }
            /*console.log('correct ', correct);
             console.log('in basket ', dropBasket);
             console.log('out basket ', outBasket);*/
            return {correct: correct, dropBasket: dropBasket, outBasket: outBasket}
        };

        //var a = $scope.result();
        //console.log(a);

        $scope.drop = function (params) {
            var dropId = params.target.dataset.dropId;
            if ($scope.drag == dropId) {

                /*if ($scope.items[$scope.drag] == undefined) {
                 $scope.items[$scope.drag] = {};
                 }*/

                $scope.items[$scope.drag]['inCorrectBasket'] = true;
                $scope.items[$scope.drag]['inBasket'] = true;

                console.log($scope.items[$scope.drag].inCorrectBasket);
            } else {
                $scope.items[$scope.drag]['inBasket'] = true;

                console.log($scope.items[$scope.drag]);
            }
        };


        $scope.out = function (params) {
            var dropId = params.target.dataset.dropId;
            if ($scope.drag == dropId) {
                console.log(params.target.dataset.dropId);
                $scope.items[dropId]['inCorrectBasket'] = false;
                $scope.items[dropId]['inBasket'] = false;
                console.log($scope.items[dropId].inCorrectBasket);
            }
            $scope.items[$scope.drag]['inBasket'] = false;
            console.log(params.target.dataset.dropId);
        };


        /*        $scope.drop = function(params) {
         if (.context.dataset.unique == 1) {
         $scope.dog = '@';
         };
         };

         $scope.out = function() {
         $scope.dog = '';
         }*/


        $scope.groups = [
            {
                'optionsList': 'newOptionsList1',
                'hard': "boxes-hard",
                'name': "box",
                'imgClass': 'img-container-box',
                'img': '../images/img/4G_2-13.png',
                'text': 'установка двигателя',
                'items': [
                    { 'num': '1', 'id': 'b1'},
                    { 'num': '2', 'id': 'b2'},
                    { 'num': '3', 'id': 'b3'},
                    { 'num': '4', 'id': 'b4'},
                    { 'num': '5', 'id': 'b5'},
                    { 'num': '6', 'id': 'b6'},
                    { 'num': '7', 'id': 'b7'},
                    { 'num': '8', 'id': 'b8'}
                ],
                'list': 'boxlist'
            },
            {
                'optionsList': 'newOptionsList2',
                'hard': "humans-hard",
                'name': "human",
                'imgClass': '',
                'img': '',
                'text': 'человек-молекула',
                'items': [
                    { 'num': '1', 'id': 'hu1'},
                    { 'num': '2', 'id': 'hu2'},
                    { 'num': '3', 'id': 'hu3'},
                    { 'num': '4', 'id': 'hu4'},
                    { 'num': '5', 'id': 'hu5'},
                    { 'num': '6', 'id': 'hu6'}
                ],
                'list': 'humanlist'
            },
            {
                'optionsList': 'newOptionsList3',
                'hard': "hexas-hard",
                'name': "hexa",
                'imgClass': 'img-container',
                'img': '../images/img/4G_2-13.png',
                'text': 'установка двигателя',
                'items': [
                    { 'num': '1', 'id': 'h1'},
                    { 'num': '2', 'id': 'h2'},
                    { 'num': '3', 'id': 'h3'},
                    { 'num': '4', 'id': 'h4'},
                    { 'num': '5', 'id': 'h5'},
                    { 'num': '6', 'id': 'h6'}
                ],
                'list': 'hexlist'
            },
            {
                'optionsList': 'newOptionsList4',
                'hard': "gears-hard",
                'name': "gear",
                'imgClass': 'img-container-gear',
                'img': '../images/img/4G_2-14.png',
                'text': 'чел в тачке',
                'items': [
                    { 'num': '1', 'id': 'g1'},
                    { 'num': '2', 'id': 'g2'},
                    { 'num': '3', 'id': 'g3'},
                    { 'num': '4', 'id': 'g4'},
                    { 'num': '5', 'id': 'g5'},
                    { 'num': '6', 'id': 'g6'}
                ],
                'list': 'gearlist'
            }

        ];

        /*        $scope.humantab = 'human';
         $scope.hextab = 'hexahedron';
         $scope.geartab = 'gear';
         $scope.boxtab = 'box';
         $scope.tab_class = 'hexahedron';

         $scope.human = function () {
         $scope.tab_class = $scope.humantab;
         };

         $scope.hex = function () {
         $scope.tab_class = $scope.hextab;
         };

         $scope.gear = function () {
         $scope.tab_class = $scope.geartab;
         };

         $scope.box = function () {
         $scope.tab_class = $scope.boxtab;
         };


         $scope.startCallback = function (name) {
         $scope.dragged_name = name;
         };*/


        /**
         * itemTypes это типы передвигаемых элементов, или groups[0].name
         * @type {string[]}
         */
        var itemTypes = ['box', 'human', 'hexa', 'gear' ];
        $scope.selectedTab = 'hexa';
        $scope.selectTab = function (tab) {
            $scope.selectedTab = tab;
        };

        $scope.optionsList = {};
        for (var i = 0; i < itemTypes.length; i++) {
            (function (e) {
                var typeName = itemTypes[e];
                var number = e + 1;
                var method = 'newOptionsList' + number;
                //console.log(e);
                //console.log(typeName);
                $scope[method] =
                {
                    accept: function (dragEl) {

                        //console.log(typeName);

                        //console.log('listId', dragEl.context.dataset.listId);
                        $scope.drag = dragEl.context.dataset.dragId;
                        if (dragEl.context.dataset.listId == typeName) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    activeClass: function (dragEl) {
                        if (true) {
                            return 'icon-hard'
                        }
                    }

                }
            })(i);
            /*var typeName = itemTypes[i];
             var number = i + 1;
             var method = 'newOptionsList' + number;*/

            /*$scope[method] =
             {
             accept: function (dragEl) {

             console.log(typeName);
             */
            /*
             console.log('listId', dragEl.context.dataset.listId);
             if (dragEl.context.dataset.listId == typeName) {
             return true;
             } else {
             return false;
             }*/
            /*
             }
             };*/

        }
        //console.log($scope.newOptionsList1);

        // Limit items to be dropped in list1
        /*$scope.optionsList1 = {
         accept: function (dragEl) {
         console.log(dragEl);
         //$scope.dragged_name = dragEl.context.dataset.listId;
         */
        /*console.log(dragEl.context.dataset.listId);
         console.log(dragEl);*/
        /*
         $scope.drag = dragEl.context.dataset.dragId;

         if (dragEl.context.dataset.listId == 'box') {
         return true;
         } else {
         return false;
         }
         }
         };*/


        for (var i = 0; i < $scope.groups.length; i++) {
            for (var j = 0; j < $scope.groups[i].items.length; j++) {
                $scope.items[$scope.groups[i].items[j].id] = {'inBasket': false, 'inCorrectBasket': false};
            }
        }


    });
